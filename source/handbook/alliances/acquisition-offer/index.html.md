---
layout: markdown_page
title: "Acquisition Offer"
---

## On this page
{:.no_toc}

- TOC
{:toc}

This page has been deprecated, please refer to our [acquisitions handbook](/handbook/acquisitions) for information on acquisitions.
