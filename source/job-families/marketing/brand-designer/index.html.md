---
layout: job_family_page
title: "Brand Designer"
---

## Brand Designer

At GitLab, Brand Designers are responsible for driving brand strategy across the company, collaborating closely with Marketing, Sales, and Product teams - helping to elevate our visual brand as we interact with the world, both through product and marketing. This role is essential in maintaining our brand guidelines across multiple platforms and critiquing deliverables with an eye for detail, craft, and consistency.

### Responsibilities

- Define and create a visual language as an extension of the brand (illustrations, icons, colors, typography, animations, etc.)
- Be an advocate for consistent and cohesive design throughout the company.
- Contribute to the [Pajamas Design System](https://design.gitlab.com/); specifically the Brand and Marketing sections.
- Educate others on design thinking, brand experience, and design-driven storytelling.
- Collaborate with Content Marketing, and others, to establish a clear and concise brand voice, tone, and personality.
- Collaborate with UX Design to create and maintain a cohesive and consistent brand experience between Marketing and Product.
- Continually iterate on concepts in an effort to make the brand relevant and relatable to our audience(s).
- Strong ability to prioritize work and resources across various projects.


### Requirements

- Effectively communicate conceptual ideas and design rationale.
- Engage in constructive design critiques.
- Able to work independently, prioritize accordingly, and iterate quickly.
- Strong communication skills without a fear of over communication.
- You share our [values](/handbook/values), and work in accordance with those values.

## Levels

### Junior Brand Designer

- Execute design and conceptual tasks as assigned with significant attention to detail.
- Learn the GitLab brand, visual language, and the [Pajamas Design System](https://design.gitlab.com/).
- Contribute to, and collaborate with fellow designers, on design concepts and the GitLab brand experience.

#### Requirements

- 1–3 years experience in a design-related role.
- Ability to continuously meet deadlines and execute on projects.

### Intermediate Brand Designer

- Develop visual concepts for digital campaigns, webpages, print, and events.
- Enable others, non-designers, to create on-brand assets.
- Actively contribute to the [Pajamas Design System](https://design.gitlab.com/).

#### Requirements

- 3–5 years experience in a design-related role.
- Proven ability to independently conceptualize and think critically about design.

### Senior Brand Designer

Everything in the Brand Designer role, plus:

- Brand vision — elevate and evolve the GitLab brand experience across multiple platforms as the company scales.
- Actively contribute to the [Pajamas Design System](https://design.gitlab.com/) and lead efforts to build out the Brand and Marketing sections.
- Analyze and interpret the impact of creative to measure, learn, and optimize.
- Manage creative work across all mediums (print, web, digital) ensuring a consistent and cohesive brand experience.
- Establish design direction (brand strategy) for various projects and campaigns in a manner that allows other designers to pick up where you left off.
- Provide clear creative direction, 1-on-1 mentorship, and timely feedback to Production Designers.

#### Requirements

- 5+ years experience in a design-related role.
- Proven ability to establish strong creative concepts and strategy.
- Strong critical thinking and storytelling skills.
- Ability to work independently, drive multiple projects, prioritize accordingly, and iterate quickly.
- Drive cross-discipline collaboration as it relates to brand experience (Sales, Content, UX, etc.).

## Manager, Creative

Everything in the Senior Brand Designer role, plus:

- Lead the Brand Design team fostering innovation, collaboration, and continual evolution of GitLab's brand experience.
- Oversee the definition, prioritization, and improvement of the Brand and Marketing sections of the [Pajamas Design System](https://design.gitlab.com/).
- Research and develop a greater understanding of GitLab's audience(s); iterating to keep our brand experience relevant and relatable.

### Requirements

- 5+ years experience in branding and marketing design.
- Degree in Graphic Design, Marketing, or equivalent work experience.
- Excellent communication, organizational, and leadership capabilities.
- Proven experience leading designers and successfully maintaining and evolving brand experience in all aspects.
